;;Alessandro Cordella
;;Matricola: 819716
;;alessandro.cordella@studio.unibo.it
globals [
  totalCells
  ;;coefficienti di adesione cellulare, dipendenti dal tipo delle due cellule a cui si riferiscono
  Jpp
  Jpq
  Jpn
  Jqn
  ;;valori limite
  thresholdOxygen
  thresholdGlucose
  ;;valori ottimali
  optimalOxygen
  optimalGlucose
  ;;costanti di diffusione
  diffusionOxygen
  diffusionGlucose
  diffusionWaste
  diffusionGrowth
  diffusionInhibitory

]

breed [cells cell]

cells-own [
  living?
  status ;;proliferating, quiscent or necrotic
  volume
  targetVolume
  cellCycleDuration
  cellClock
  id
  oxygenConsumptionRate
  glucoseConsumptionRate
  wasteConsumptionRate
  growthFactorsConsumptionRate
  inhibitoryFactorsConsumptionRate
  localOxygen
  localGlucose
  localWaste
  localGrowth
  localInhibitory
  ;;controllano lo stato delle cellule quiescenti
  dtimer
  dtimer2
]

patches-own [
  Oxygen
  Glucose
  Growth
]

to setup
  clear-all
  reset-ticks
  set Jpp 28
  set Jpq 28
  set Jpn 24
  set Jqn 22
  set thresholdOxygen 0.02
  set thresholdGlucose 0.06
  set optimalOxygen 0.28
  set optimalGlucose 5.5
  set diffusionOxygen 5.94 * 10 ^ -2
  set diffusionGlucose 1.52 * 10 ^ -3
  set diffusionWaste 2.124 * 10 ^ -3
  set diffusionGrowth 10 ^ -6

  ask patches [
    set pcolor 48
    set Oxygen optimalOxygen
    set Glucose optimalGlucose
    set Growth 1
  ]
  create-cells 1 [
    set shape "circle"
    set color green
    set living? true
    set size initialCellSize
    set status "proliferating"
    set volume (4 / 3) * pi * (size / 2)^ 2
    set targetVolume volume * 2
    set heading random 360
    set id who
    set oxygenConsumptionRate setDefaultRate status "Oxygen"
    set glucoseConsumptionRate setDefaultRate status "Glucose"
    set wasteConsumptionRate setDefaultRate status "Waste"
    set growthFactorsConsumptionRate setDefaultRate status "Growth"
    set inhibitoryFactorsConsumptionRate setDefaultRate status "Inh"
    set localOxygen 1
    set localGlucose 1
    set localWaste 1
    set localGrowth 1
    set localInhibitory 1
    set cellCycleDuration 12
    set cellClock 0
    set dtimer 0
    set dtimer2 0
    set totalCells totalCells + 1
  ]
end

;;ritorna l'energia dell'interazione di due cellule
to-report energy [id1 id2]
  let H 0
  let s1 0
  let s2 0
  let v1 0
  let v2 0
  let tv1 0
  let tv2 0
  let J 1
  let K 0 ;;kronecker
  let firstTerm 0
  let El1 0
  let El2 0
  let secondTerm 0
  ask cells [
    if id = id1 [
      set s1 status
      set v1 volume
      set tv1 targetVolume
    ]
    if id = id2 [
      set s2 status
      set v2 volume
      set tv2 targetVolume
    ]
  ]
  if ((s1 = "proliferating")AND(s2 = "proliferating"))  [ set J Jpp ]
  if (((s1 = "proliferating")AND(s2 = "quiescent"))OR((s1 = "quiescent")AND(s2 = "proliferating")))  [ set J Jpq ]
  if (((s1 = "proliferating")AND(s2 = "necrotic"))OR((s1 = "necrotic")AND(s2 = "proliferating")))  [ set J Jpn ]
  if (((s1 = "quiescent")AND(s2 = "necrotic"))OR((s1 = "necrotic")AND(s2 = "quiescent")))  [ set J Jqn ]

  ifelse id1 != id2 [set K 0] [set K 1]

  set firstTerm (initialCellSize * 27 * 2) * (J * (1 - K))

  set El1 (random (3 - 1) + 1 ) ;;numero casuale tra 1 e 3
  set El2 (random (3 - 1) + 1 )

  set secondTerm (El1 * (v1 - tv1)^ 2) + (El2 * (v2 - tv2)^ 2)

  set H firstTerm + secondTerm
  report H
end


to go
  ifelse ((all? cells [status = "necrotic"])OR((not (any? cells with [status = "proliferating"]))AND(not(any? cells with [status = "quiescent"])))) [
    stop
  ]
  [
    let changeCounter 0
    diffuse Oxygen 1
    diffuse Glucose 1
    diffuse Growth 1
    ifelse totalCells > 0 [
      ;;1/4 Monte Carlo Step
      while [changeCounter < totalCells / 4] [
        let cellToDivide nobody
        ask cells [
          let rad 4
          let neighbor nobody
          let p 0
          let idn 0
          ;;aggiorna il numero delle cellule
          ifelse totalCells > 1 [
            while [neighbor = nobody] [
               set neighbor one-of other cells in-radius (patch-size / rad) with [living? = true]
               set rad rad / 2
               if ((rad < 1)AND(neighbor = nobody)) [stop]
            ]
            ask neighbor [
              set idn id
            ]
            set p acceptingChange id idn
            ;;accetta o meno il cambiamento
            if ((p = 1)OR(random-float 1 < p)) [
              let tmp id
              set id idn
              set idn tmp
              set changeCounter changeCounter + 1
            ]
          ]
          [

            set changeCounter changeCounter + 0.01
          ]
          ;;Vengono risolve le equazioni chimiche, controllato il livello fattoriale e il ciclo cellulare
          if status != "necrotic" [
            solveChemicalEquations self
            let fl calculateFactorLevel self
            let div 0
            ifelse ((fl = 1)OR(random-float 1 < fl)) [
              ;;posso aumentare di volume solo se sono una cellula proliferating
              if status = "proliferating" [
                ;;ingrandimento
                let tmpSize size + (size / 16)
                let tmpVolume (4 / 3) * pi * (tmpsize / 2)^ 2
                set cellClock cellClock + 2
                ifelse tmpvolume < targetVolume[
                  set size tmpSize
                  set volume tmpVolume
                  set div 0
                ] [
                  set div 1 ;;questa cella potrebbe essere divisa

                ]
              ]
              ;;E' passato abbastanza tempo per dividere?
              ifelse cellClock >= cellCycleDuration [
                ifelse div = 1 [
                  set cellToDivide self
                ]
                [
                 ;;la cellula ha fatto un ciclo ma il suo volume non si è raddoppiato, quindi non faccio niente
                  set cellClock 0
                ]
              ] [
                ifelse div = 1 [
                  setQuiescent self
                ] [
                  ;;continua
                ]
              ]
            ]
            [
              setQuiescent self
            ]
          ]

        ]
        if cellToDivide != nobody [
          divide cellToDivide
        ]
        if (ticks > 150 and ((count cells with [status = "quiescent"] < 4 )or (count cells with [status = "proliferating"] < 4 ))) [
          ask cells [
            setNecrotic self
          ]
        ]
      ]
      tick
    ]
    [
    ;;nessuna cellula
    ]
    ;;gestione grafico
    set-current-plot "CellPopulation"
    set-current-plot-pen "proliferating"
    plot count cells with [ status = "proliferating" ]
    set-current-plot-pen "quiescent"
    plot count cells with [ status = "quiescent" ]
    set-current-plot-pen "necrotic"
    plot count cells with [ status = "necrotic" ]
  ]

end

;;ritorna il valore su cui si basa il cambiamento della cellula
to-report acceptingChange [id1 id2]
  let h1 0
  let h2 0
  let deltaH 0
  set h1 energy id1 id2
  set h2 energy id2 id1
  set deltaH (h2 - h1)
  ifelse deltaH < 0 [
    report 1
  ] [
    let bolz 1.38 * (10 ^ -23)
    let t ((size * 20) / 100) ;;20% del diametro della cellula
    report (e ^ ((- deltaH) / (bolz ^ t)))
  ]
end

;;divide una cellula
to divide [cel]
  let newId totalCells
  let newColor 0
  let newSize 0
  let newStatus 0
  let newVolume 0
  let newTargetVolume 0
  let newO 0
  let newGl 0
  let newW 0
  let newGr 0
  let newI 0
  let newX 0
  let newY 0
  ask cel [
    set volume volume / 2
    set targetVolume volume * 2
    set size sqrt ((3 * volume) / Pi )
    set cellClock 0

    set newColor color
    set newStatus status
    set newVolume volume
    set newTargetVolume newVolume * 2
    set newSize size
    set newO setDefaultRate status "Oxygen"
    set newGl setDefaultRate status "Glucose"
    set newW setDefaultRate status "Waste"
    set newGr setDefaultRate status "Growth"
    set newI setDefaultRate status "Inh"
    set newX xcor
    set newY ycor
  ]
 create-cells 1 [
    set shape "circle"
    set living? true
    set id newId
    set color newColor
    set size newSize
    set status newStatus
    set volume newVolume
    set targetVolume newTargetVolume
    set oxygenConsumptionRate newO
    set glucoseConsumptionRate newGl
    set wasteConsumptionRate newW
    set growthFactorsConsumptionRate newGr
    set inhibitoryFactorsConsumptionRate newI
    set xcor newX
    set ycor newY
    set cellCycleDuration 12
    set cellClock 0
    set dtimer ticks
    set dtimer2 0
    set heading random 360
    ;;setHeading self
    jump size

    ifelse status = "proliferating" [
      if ((count (cells-on self) with [status != "proliferating"]) > 0) [
        ask self [die]
      ]
    ] [
      if status = "quiescent" [
        if ((count (cells-on self) with [status = "necrotic"]) > 0) [
          ask self [die]
        ]
      ]
    ]

  ]
  set totalCells totalCells + 1
end

;;setta i valori di default di alcuni parametri
to-report setDefaultRate [stat typ]
  ifelse stat = "proliferating"[
    ifelse typ = "Oxygen" [report 108] [
      ifelse typ = "Glucose" [report 162] [
        ifelse typ = "Waste" [report 240] [
          ifelse typ = "Growth" [report 1] [
            report 0
          ]
        ]
      ]
    ]
  ][
    ifelse stat = "quiescent" [
      ifelse typ = "Oxygen" [report 50] [
      ifelse typ = "Glucose" [report 80] [
        ifelse typ = "Waste" [report 110] [
          ifelse typ = "Growth" [report 0.5] [
            report 1
          ]
        ]
      ]
    ]
    ] [
      ifelse typ = "Oxygen" [report 0.1] [
        ifelse typ = "Glucose" [report 0.1] [
          ifelse typ = "Waste" [report 0.1] [
            ifelse typ = "Growth" [report 0.1] [
              report 2
            ]
          ]
        ]
      ]
    ]
  ]
end

;;aggiorna il valore di alcuni parametri della cellula
to-report changeRate [stat typ u]
  let originalO setDefaultRate stat "Oxygen"
  let originalG setDefaultRate stat "Glucose"
  let originalW setDefaultRate stat "Waste"
  let a2 (originalO * round ((u - thresholdOxygen) / (optimalOxygen - thresholdOxygen)))
  let b2 (originalG * round ((u - thresholdGlucose) / (optimalGlucose - thresholdGlucose)))
  if typ = "Oxygen" [
    report a2
  ]
  if typ = "Glucose" [
    report b2
  ]
  if typ = "Waste" [
    report (originalW * (((a2 / originalO) + (b2 / originalG)) / 2))
  ]
  report 1 ;;growth or inhi
end

;;regola il metabolismo della cellula
to solveChemicalEquations [cel]
  ask cel [
    let change 0
    let d1 0
    let ox 0
    let d2 0
    let gl 0
    set localOxygen (diffusionOxygen * localOxygen + oxygenConsumptionRate);;valore ritornato dalle equazioni
    set localGlucose (diffusionGlucose * localGlucose + glucoseConsumptionRate)
    set localWaste (diffusionWaste * localWaste + wasteConsumptionRate )
    set localGrowth (diffusionGrowth * localGrowth + growthFactorsConsumptionRate)
    set localInhibitory (diffusionInhibitory * localInhibitory + inhibitoryFactorsConsumptionRate)
    set oxygenConsumptionRate changeRate status "Oxygen" localOxygen ;;valore ritornato da change rate
    set glucoseConsumptionRate changeRate status "Glucose" localGlucose
    set wasteConsumptionRate changeRate status "Waste" localWaste
    ask patch-at xcor ycor [
      set Oxygen Oxygen - 0.015
      set Glucose Glucose - 0.55
      if ((Oxygen < thresholdOxygen)OR(Glucose < thresholdGlucose)) [
        set change 1
      ]
    ]
    ifelse change = 1 [
      ifelse status = "proliferating" [
          setNecrotic cel
        ] [
          ;;se una cella è proliferating diventerà necrotic, se è quiescent rimarrà quiescent, se è necrotic, non può cambiare
          setQuiescent cel
          if status = "quiescent" [
            set dtimer2 ticks
            if dtimer2 - dtimer > 25 [
              setNecrotic self
            ]
          ]
        ]
    ]
    [
    ]

  ]
end

;;ritorna il factor level di una cellula
to-report calculateFactorLevel [cel]
  let factor 0
  let gF 0
  let ihF 0
  let initGF 0
  let thre 1
  ask cel [
    set gF localGrowth
    set ihF localInhibitory
    ask cel [
      ask patch-at xcor ycor [
        set initGF Growth
        set Growth Growth - 0.0035
      ]
    ]
  ]
  set factor (1 + e ^ -1 * (((gF - ihF) / initGF) - thre)) ^ -1
  report factor
end

;;setta una cellula quiescente
to setQuiescent [cel]
  ask cel [
    if status != "necrotic" [
      set color red
      set status "quiescent"
    ]

  ]
end

;;sette una cellula necrotica
to setNecrotic [cel]
  ask cel [
    set color grey
    set status "necrotic"
    set living? false
  ]
end

;;setta la posizione di una cella
to setHeading [cel]
  ask cel [
    ifelse (xcor = 0 and ycor = 0) [
      set heading random 360
    ]
    [
      ifelse (xcor >= 0 and ycor >= 0) [
        let random-list 0
        set random-list list (random (180 - 0 + 1) + 0) (random (360 - 270 + 1) + 270)
        set heading one-of random-list
      ]
      [
        ifelse (xcor <= 0 and ycor >= 0 ) [
         set heading random (270 - 0 + 1) + 0
        ]
        [
          ifelse (xcor <= 0 and ycor <= 0) [
            set heading random (360 - 90 + 1) + 90
          ]
          [
            let random-list 0
            set random-list list (random (90 - 0 + 1) + 0 ) (random (360 - 180 + 1) + 180 )
            set heading one-of random-list
          ]
        ]
      ]
    ]
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
320
14
757
452
-1
-1
13.0
1
10
1
1
1
0
1
1
1
-16
16
-16
16
0
0
1
ticks
30.0

BUTTON
14
53
229
86
Setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
12
144
235
177
EvolveCellularModel-1 step
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
968
15
1457
450
CellPopulation
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"proliferating" 1.0 0 -13840069 true "" "plot count cells with [ status = \"proliferating\" ]"
"quiescent" 1.0 0 -2674135 true "" "plot count cells with [ status = \"quiescent\" ]"
"necrotic" 1.0 0 -7500403 true "" "plot count cells with [ status = \"necrotic\" ]"

BUTTON
12
200
236
233
EvolveCellularModel
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
13
98
231
131
initialCellSize
initialCellSize
1
4
2.0
0.1
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
